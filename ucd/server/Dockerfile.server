# (C) Copyright Zilker Technology 2018.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
FROM ibmjava:8-sfj

LABEL copyright="(c) Copyright Zilker Technology 2018. All Rights Reserved."
LABEL maintainer="David J. Arnone <darnone@ztech.io>"

RUN apt-get update
RUN apt-get -y install libaio1 libnuma-dev curl unzip vim

EXPOSE 8443
EXPOSE 8080
EXPOSE 7918
EXPOSE 7919

# Copy the files to tmp folder
ADD ./common /tmp/
ADD ibm-ucd*.zip /tmp

# Capture arguments from docker command line or docker-compose
# Set defaults which can be overridden
ARG UCD_SERVER_HOME=/opt/IBM/ibm-ucd/server
ARG UCD_DB_TYPE=derby

# Inject environment variables into image and container
ENV UCD_SERVER_HOME=$UCD_SERVER_HOME
ENV UCD_DB_TYPE=$DB_TYPE

WORKDIR /tmp

# create an entrypoint command script using $UCD_SERVER_HOME
RUN echo "$UCD_SERVER_HOME/bin/server run" > entrypoint-ibm-ucd-server.sh

# Install UCD Server non-interactively
RUN /bin/sh setup-ibm-ucd-server.sh

# Set container startup command
ENTRYPOINT ["/bin/bash", "entrypoint-ibm-ucd-server.sh"]

