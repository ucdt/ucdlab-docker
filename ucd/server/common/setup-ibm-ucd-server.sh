#!/bin/sh
# (C) Copyright Zilker Technology 2018.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Install a UCD Server
# Set the following environment variables (ENV) when running for the first time
# DB_TYPE: The type of database to build for. Empty value will default to derby

timestamp=$(date)
echo "Started building IBM UrbanCode Deploy Server container image at $timestamp."

# unzip install image
if ls /tmp/ibm-ucd*.zip 1> /dev/null 2>&1; then
    echo unzip -q /tmp/ibm-ucd*.zip -d /tmp
    unzip -q /tmp/ibm-ucd*.zip -d /tmp
    #rm /tmp/ibm-ucd*.zip
else
    echo "No install image found. /tmp/ibm-ucd*.zip"
    exit 1
fi

# determine type of database and set supplemental properties
if [ "$UCD_DB_TYPE" = "mysql" ]; then
    echo "[Database] Configuring MYSQL"
    cp /tmp/supplemental-mysql-install.properties /tmp/supplemental-install.properties
    #installing mysql connector jar
    apt-get install libmysql-java -y
    cp /usr/share/java/mysql.jar /tmp/ibm-ucd-install/lib/ext/mysql.jar
else
    echo "[Database] Configuring Derby"
    cp /tmp/supplemental-derby-install.properties /tmp/supplemental-install.properties
fi

# set install properties
cat /tmp/supplemental-install-common.properties >>/tmp/ibm-ucd-install/install.properties
cat /tmp/supplemental-install.properties >> /tmp/ibm-ucd-install/install.properties
echo "" >> /tmp/ibm-ucd-install/install.properties
echo "install.server.dir=$UCD_SERVER_HOME" >> /tmp/ibm-ucd-install/install.properties

timestamp=$(date)
echo "Completed preparing IBM UrbanCode Deploy Server property files at $timestamp."

# install the server
cd /tmp/ibm-ucd-install/
./install-server.sh

timestamp=$(date)
echo "Completed IBM UrbanCode Deploy Server container install at $timestamp."

# Clean up unused rcl files
rm -rf $UCD_SERVER_HOME/lib/rcl/aix
rm -rf $UCD_SERVER_HOME/lib/rcl/hp-risc
rm -rf $UCD_SERVER_HOME/lib/rcl/itanium
rm -rf $UCD_SERVER_HOME/lib/rcl/linux-ppc
rm -rf $UCD_SERVER_HOME/lib/rcl/mac
rm -rf $UCD_SERVER_HOME/lib/rcl/solaris
rm -rf $UCD_SERVER_HOME/lib/rcl/solaris-sparc
rm -rf $UCD_SERVER_HOME/lib/rcl/win

# Clean up temporary install files
rm -rf /tmp/supplemental*
rm -rf /tmp/ibm-ucd-install /tmp/ibm-ucd*.zip

timestamp=$(date)
echo "Finished building IBM UrbanCode Deploy Server container image at $timestamp."
