#!/bin/sh
# (C) Copyright Zilker Technology 2018.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# agent.id and agent.name are removed from installed.properties file.
sed -i '/agent.id/d' $AGENT_HOME/conf/agent/installed.properties
sed -i '/agent.name/d' $AGENT_HOME/conf/agent/installed.properties

# agent.name is set to something unique - append hostname.
agentName="agent-"$HOSTNAME

# AGENT_NAME - set in the Dockerfile and overrules a unique agent.name
if [ $AGENT_NAME ]; then
    agentName=$AGENT_NAME
fi

# Setting agent.name to value specified in the Dockerfile as AGENT_NAME
# For more than one agent, unset AGENT_NAME in Dockerfile to make the
# agent name unique - ie "agent-<hostname_of_container>"
echo "locked/agent.name="$agentName >> $AGENT_HOME/conf/agent/installed.properties

#Setting the agent.id to "" so that id is auto generated during start
agentId=""
echo "locked/agent.id="$agentId >> $AGENT_HOME/conf/agent/installed.properties

echo "Starting the agent now"
$AGENT_HOME/bin/agent run
