#!/bin/sh
# (C) Copyright Zilker Technology 2018.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Install UCD Agent
# Set the following environment variables when running
# ucdInstallImageUrl = The URL of the install image zip file, or null if your build has already placed it in /tmp
# credentials = The userid:password needed to download $ucdInstallImageUrl, or null if none needed.
# AGENT_TYPE = set to web or jms in in the Dockerfile 

timestamp=$(date)
echo "Started building IBM UrbanCode Deploy Agent container image at $timestamp."
# unzip the ucd files and agent files
if ls /tmp/ibm-ucd*.zip 1> /dev/null 2>&1; then
    echo unzip -q /tmp/ibm-ucd*.zip -d /tmp
    unzip -q /tmp/ibm-ucd*.zip -d /tmp
    rm /tmp/ibm-ucd*.zip
else
    echo "No install image found. /tmp/ibm-ucd*.zip"
    exit 1
fi

# set install properties
if [ "x$AGENT_TYPE" = "xweb" ]; then
    echo "Installing a web agent!"
    cat /tmp/install-web.properties >> /tmp/ucd-agent-install/install.properties
elif [ "x$AGENT_TYPE" = "xjms" ]; then
    echo "Installing a JMS agent!"
    cat /tmp/install-jms.properties >> /tmp/ucd-agent-install/install.properties
else 
    echo "Unrecognized agent type. Aborting..."
    exit 1
fi

# change to installer directory
cd /tmp/ucd-agent-install

timestamp=$(date)
echo "Completed preparing IBM UrbanCode Deploy Agent install files at $timestamp."

# install the agent
bash ./install-agent-from-file.sh install.properties

timestamp=$(date)
echo "Completed IBM UrbanCode Deploy Agent install at $timestamp."

#Clean up temporary install files
#rm -rf /tmp/ibm-ucd-agent-install

timestamp=$(date)
echo "Finished building IBM UrbanCode Deploy Agent container image at $timestamp."
