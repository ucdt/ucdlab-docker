# ucdlab-docker

This repository contais the Docker and Docker Compose source code for constructing a UCD Training Lab environment.
This includes containers for Bitbucket, Articfactory OSS, Artifactory Pro, Jenkins Home Data, Jenkins Master, UCD
Server, and UCD Agent.  The UCD Relay is part of the code base but has been copied from public implememntiton from IBM 
for completenes as is. It has not been upgraded nor is included on any of the upper level Compose files.

The containers are deployed as part a Docker Comprose project called ucdlab. As a result, the names for containers,
networksm adn volunes are prepended with 'ucdlab_'. For example, ucdlab_ucd-server_1. Additionally, all of the containers
share a common netowrk -ucd-net. If resolvin services from a brower external to the Docker host, the URL should use the 
IP of the Docker host. This can be simplified with local DNS by adding the Docker host to your /etc/hosts file. Each
container can be deployed individually using Docker or Docker Compose,however, there is a top level Docker Compose file
that will build the entire lab.

The UCD Server and Agent install packages have been omitted from the repository. They can be obtained from IBM Passport
Advantage, IBM PartnerWorld, or IBM Fix Central. The implementation currently uses IBM UrbanCode Deploy Version 7.0.0.1.
but later versions of UCD should work just fine. If later versions of UCD are to be used, the Docker commands and the
Docker Compose files should be change to reflect the UCD version.

For the UCD Agent, a new higher performing web agent was introduced in UCD V7.X but ther is still support for the legacy 
JMS agent. The configuration of the UCD Agent in Docker defaults to the web agent but ths can be change to JMS by modifying
the .env file at the level of structure from which Docker Compose is executed. To persist downloaded artifacts, a property 
should be added to the UCD Agent. The lab documentation uses WORKING_DIR=/mnt/working_dir. This property is then prepended
to all working directory definitions a ${p:agent/working_dir}.

The Administration Guide located in a speaate repository provides all of the instructions for building and deploying the
lab environment. The Docker and Docker Compose API used is3.7. The Docker and Docker Compose version used to contruct the
lab are docker version 18.06.1-ce, build e68fc7a and docker-compose version 1.22.0, build f46880fe respectively.
